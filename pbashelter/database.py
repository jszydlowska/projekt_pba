from uuid import UUID, uuid1
from models import CrateDoesNotExistException, DogAlreadyExistsException,\
  DogOrCrateDoesNotExistException, DogAlreadyAssignedToThisCrate, DogDoesNotExistException, CrateIsFullException,\
    CrateMustBeEmptyForNewDogException

class ShelterDatabase:
  def __init__(self):
    self.dogs = dict()
    self.crates = dict()
    self.credentials = dict(
      username="zwierzaki",
      password="zwierzaki"
    )

  # Add dog to the databse
  def addDog(self, dog):
    if dog.crateId != None:
      raise CrateMustBeEmptyForNewDogException

    if dog.id in self.dogs.keys():
      raise DogAlreadyExistsException

    self.dogs[dog.id] = dog
    return True
  #
  
  # Return list of all dogs
  def getDogsList(self):
    return [self.dogs[key] for key in self.dogs.keys()]

  # Modify existing dog in the databse
  def modifyDog(self, dog, dogId):
   if dog.id == dogId:
     if dog.id in self.dogs.keys():
       self.dogs[dog.id] = dog
       return self.dogs[dog.id]
     else:
       #dog does not exist
       return False
  #

  def deleteDog(self, id):
    # Should return something saying that dog has been deleted
    if id in self.dogs.keys():
      if self.dogs[id].crateId != None:
        self.crates[self.dogs[id].crateId].num_of_dogs -= 1
      del self.dogs[id]
      return True
    else:
      #dog does not exist
      return False
  #

  ########## CRATES ###########

  # Return list of all crates
  def getCratesList(self):
    return [self.crates[key] for key in self.crates.keys()]


  # Add dog to the databse
  def addCrate(self, crate):
    if crate.id not in self.crates.keys():
      self.crates[crate.id] = crate
      return True
    else:
      return False
  #

  # Get dogs from specific crate
  def getDogsInCrate(self, crateId):
    # Check if dog exists
    xxx = []
    if crateId in self.crates.keys():
      for dogId in self.dogs.keys():
        if crateId == self.dogs[dogId].crateId:
          xxx.append(self.dogs[dogId])
      return xxx
    else:
      # dog does not exist
      return []


  # Modify existing dog in the databse
  def addDogToCrate(self, dogId, crateId):
    if dogId not in self.dogs.keys():
      raise DogDoesNotExistException
    if crateId == self.dogs[dogId].crateId:
      raise DogAlreadyAssignedToThisCrate
    if self.crates[crateId].num_of_dogs >= self.crates[crateId].size:
      raise CrateIsFullException

    self.dogs[dogId].crateId = crateId
    self.crates[self.dogs[dogId].crateId].num_of_dogs += 1
    return True
  #

  # Delete dog from the crate so it is not assigned to any
  def deleteDogFromCrate(self, crateId, dogId):
    # Check if dog exists
    if crateId in self.crates.keys():
      for dogId in self.dogs.keys():
        if crateId == self.dogs[dogId].crateId:
          self.dogs[dogId].crateId = None
          self.crates[crateId].num_of_dogs -= 1

          if self.crates[crateId].num_of_dogs < 0:
            self.crates[crateId].num_of_dogs = 0
      return True
    else:
      # dog does not exist 
      return DogOrCrateDoesNotExistException
  #